var x11 = require('x11');

var strutX;

module.exports.getDisplays = function(cb) {
    x11.createClient(function(err, display) {
        if (!err) {
            var X = display.client;
            var root = display.screen[0].root;
            X.require('randr', function(err, Randr) {
                if (!err) {
                    Randr.GetScreenResources(display.screen[0].root, function(err, scrres) {
                        if (!err) {
                            scrres.crtcs.forEach(crtc => {
                                Randr.GetCrtcInfo(crtc, scrres, function(err, crtcinfo) {
                                    if (!err) {
                                        if (crtcinfo.output.length != 0) {
                                            Randr.GetOutputInfo(crtcinfo.output[0], scrres, function(err, outputinfo) {
                                                if (!err) {
                                                    var result = {
                                                        name: outputinfo.name.replace(/\0/g, ''),
                                                        x: crtcinfo.x,
                                                        y: crtcinfo.y,
                                                        width: crtcinfo.width,
                                                        height: crtcinfo.height
                                                    }
                                                    cb(result);
                                                    X.terminate();
                                                }
                                            });
                                        }
                                    }
                                });
                            });
                        }
                    });
                }
            });
            X.on('error', function(e) {
                console.log(e);
            });
        } else {
            console.log(err);
        }
    });
};

// Functions for reserving place on the screen.
// Thank you:
//      https://stackoverflow.com/questions/43888114/how-to-set-x-properties-in-order-to-create-a-desktop-status-bar-with-electron
//      https://specifications.freedesktop.org/wm-spec/1.3/ar01s05.html

var getWindowName = function(wid) {
    return new Promise(function(resolve, reject) {
        strutX.InternAtom(false, '_NET_WM_NAME', function(wmNameErr, wmNameAtom) {
            strutX.InternAtom(false, 'UTF8_STRING', function(utf8err, utf8Atom) {
                strutX.GetProperty(0, wid, wmNameAtom, utf8Atom, 0, 10000000, function(err, nameProp) {
                    if (err) {
                        reject(err);
                    }
                    resolve(nameProp.data.toString());
                });
            });
        });
    });
}

module.exports.getWindowID = function(name) {
    return new Promise(function(resolve, reject) {
        x11.createClient(function(err, display) {
            strutX = display.client;
            var root = display.screen[0].root;
            strutX.QueryTree(root, function(err, tree) {
                tree.children.map(function(id) {
                    let prop = getWindowName(id).then(function(n) {
                        if (n === name) {
                            resolve(id);
                        }
                    });
                });
            });
        });
    });
}

module.exports.setStrutValues = function(wid,
    left, right, top, bottom, 
    left_start_y, left_end_y, right_start_y, right_end_y, 
    top_start_x, top_end_x, bottom_start_x, bottom_end_x, cb) {
    
    var values = new Buffer(4 * 12);
    values.writeUInt32LE(left           ,0*4 );
    values.writeUInt32LE(right          ,1*4 );
    values.writeUInt32LE(top            ,2*4 );
    values.writeUInt32LE(bottom         ,3*4 );
    values.writeUInt32LE(left_start_y   ,4*4 );
    values.writeUInt32LE(left_end_y     ,5*4 );
    values.writeUInt32LE(right_start_y  ,6*4 );
    values.writeUInt32LE(right_end_y    ,7*4 );
    values.writeUInt32LE(top_start_x    ,8*4 );
    values.writeUInt32LE(top_end_x      ,9*4 );
    values.writeUInt32LE(bottom_start_x ,10*4 );
    values.writeUInt32LE(bottom_end_x   ,11*4 );

    strutX.InternAtom(false, '_NET_WM_STRUT_PARTIAL', function(err, strutAtom) {
        strutX.ChangeProperty(0, wid, strutAtom, strutX.atoms.CARDINAL, 32, values);
        cb();
    });
}