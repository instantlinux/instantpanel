import { remote, ipcRenderer } from 'electron';
import * as path from 'path';
import * as fs from 'fs';
import mkdirp from 'mkdirp';
import bettersqlite3 from 'better-sqlite3';
const app: Electron.App = remote.app;

let panelid = -1;

let pageloaded = false;
ipcRenderer.on('init', function(event: any, data: any) {
    // this function will be called when we have the panel id
    panelid = data;
    if (pageloaded) initUI();
});
ipcRenderer.on('editMode', function(event: any, data: any) {
    // show or hide settings
    if (data) {
        showSettings();
    } else {
        hideSettings();
    }
})
function loadEvent() {
    // this function will be called when the page is loaded...
    divMainArea  = document.getElementById("mainarea")!;
    listPopup = document.getElementById("popup")!;
    pageloaded = true;
    if (panelid != -1) initUI();
}



declare var interact: any;

let divMainArea: HTMLElement;
let listPopup: HTMLElement;

// Search paths for widgets
const localsearchpath = path.join(app.getPath('home'), '.instantpanel/widgets');
const searchpaths = [
    '/usr/share/instantdesktop/instantpanel/widgets',
    localsearchpath
];

let db: bettersqlite3;
let isvertical = false;

// Widgets selected by the user
let widgets: Array<PanelWidget> = [];

interface PanelWidget {
    widgetName: string;
    widgetPosition: number;
    widgetWidth: number;
    domid: string;
    widgetFolder: string;
}


function initUI() {
    // this function will be called when the page is loaded AND we have the panel id
    // ---
    // Create local widget search path if it doesn't exist
    if (!fs.existsSync(localsearchpath)) {
        mkdirp(localsearchpath, function(err) {
            if (err) console.error(err);
            else readdb();
        })
    } else readdb();
    console.log("Initializing ui... " + panelid);
}

function readdb() {
    // Open the sqlite-db
    var configpath = path.join(app.getPath("appData"), "instantpanel.db");

    db = new bettersqlite3(configpath);
    
    // Check if this is a vertical panel.
    let resultpos = db.prepare("SELECT * FROM panels WHERE tbl='" + panelid + "'").all();
    if (resultpos[0].position == 2 || resultpos[0].position == 3) {
        isvertical = true;
        document.body.className = "isvertical";
    } else { document.body.className = "ishorizontal"; }
    // Set the background color
    document.querySelector("body")!.style.backgroundColor = resultpos[0].background;

    // Read the db and create an array containing our widgets.
    let result = db.prepare("SELECT * FROM panel" + panelid).all();
    result.forEach(row => {
        widgets.push({
            widgetName: row.widget,
            widgetPosition: parseInt(row.position),
            widgetWidth: parseInt(row.width),
            domid: '',
            widgetFolder: ''
        })
    });

    drawWidgets();
    getAvailableWidgets(); // we need this for our settings
}

async function drawWidgets() {
    // Iterate through the widgets
    let i = 0;
    widgets.forEach(widget => {
        let widgetFolder = "";
        // Iterate through the search paths
        searchpaths.forEach(searchpath => {
            if (fs.existsSync(path.join(searchpath, widget.widgetName, "package.json"))) {
                widgetFolder = path.join(searchpath, widget.widgetName);
            }
        });

        if (widgetFolder != "") {
            // Generate the widget and add it to the panel.
            widget.widgetFolder = widgetFolder;
            import(widgetFolder).then(async widgetGenerator => {
                i++;
                widget.domid = widget.widgetName + i;
                let domelement = document.createElement("div");
                domelement.id = widget.domid;

                if (!isvertical) {
                    domelement.style.width = widget.widgetWidth + "%";
                    domelement.style.left = widget.widgetPosition + "%";
                    domelement.style.top = "0";
                    domelement.style.bottom = "0";
                } else {
                    domelement.style.height = widget.widgetWidth + "%";
                    domelement.style.top = widget.widgetPosition + "%";
                    domelement.style.left = "0";
                    domelement.style.right = "0";
                }

                domelement.style.position = "absolute";

                domelement.appendChild(await widgetGenerator["default"](widgetFolder, isvertical));
                divMainArea.appendChild(domelement);
            }).catch(error => {
                console.error("Cannot load module! " + widget.widgetName);
                console.log(error);
            });
        } else {
            console.error("Widget not found: " + widget.widgetName);
        }
    });
}

let availableWidgets: Array<string> = [];

function getAvailableWidgets() {
    availableWidgets = [];
    listPopup.innerHTML = "";
    // Get every widget that is located in one of our search paths...
    searchpaths.forEach(searchpath => {
        fs.readdirSync(searchpath).forEach(searchitem => {
            if (fs.existsSync(path.join(searchpath, searchitem, "package.json")) && !availableWidgets.includes(searchitem)) {
                availableWidgets.push(searchitem);
            }
        });
    });

    drawItems();
}

let widgetssettings: Array<widgetSetting> = [];
let overlayshown = false;

interface widgetSetting {
    widgetName: string;
    widgetPosition: number;
    widgetWidth: number;
    domid: string;
    deleted?: boolean;
}

function drawItems() {
    // Draw a list of available widgets
    availableWidgets.forEach(widget => {
        let item = document.createElement("li");
        item.innerHTML = widget;

        listPopup.appendChild(item);

        item.onclick = function() {
            // Click event -> Add the widget to the panel.
            divMainArea.innerHTML = "";
            widgetssettings.push({
                widgetName: widget,
                widgetPosition: 0,
                widgetWidth: 10,
                domid: ''
            });
            drawwidgetssettings(); // Redraw the settings...
        };
    });
}

function togglePopup() {
    // Just a few fancy animations...
    if (!overlayshown) {
        getAvailableWidgets();
        document.getElementById("screenoverlay")!.className = "screenoverlaypopin";
    } else {
        document.getElementById("screenoverlay")!.className = "screenoverlaypopout";
    }
    overlayshown = !overlayshown;
}

function showSettings() {
    widgetssettings = [];
    document.getElementById("addbtn")!.style.display = "block";
    divMainArea.innerHTML = "";
    readdbsettings();
}

function hideSettings() {
    applychanges();
    document.getElementById("addbtn")!.style.display = "none";
    widgets = [];
    divMainArea.innerHTML = "";
    readdb();
}

function readdbsettings() {
    // Read the database into a 'widgetssettings'-array.
    let result = db.prepare("SELECT * FROM panel" + panelid).all();
    result.forEach(row => {
        widgetssettings.push({
            widgetName: row.widget,
            widgetPosition: parseInt(row.position),
            widgetWidth: parseInt(row.width),
            domid: '',
            deleted: false
        })
    });

    drawwidgetssettings();
}

function drawwidgetssettings() {
    // Draw the widgets in a unique, cool "settings-style" :)
    let i = 0;
    widgetssettings.forEach(widget => {
        i++;
        widget.domid = widget.widgetName + i;
        let domelement: HTMLElement = document.createElement("div");
        domelement.id = widget.domid;

        if (!isvertical) {
            domelement.style.width = widget.widgetWidth + "%";
            domelement.style.top = "0";
            domelement.style.left = widget.widgetPosition + "%";
            domelement.style.bottom = "0";
        } else {
            domelement.style.height = widget.widgetWidth + "%";
            domelement.style.left = "0";
            domelement.style.top = widget.widgetPosition + "%";
            domelement.style.right = "0";
        }

        domelement.style.position = "absolute";
        domelement.style.background = "rgba(190, 190, 190, 0.2)";
        domelement.style.boxShadow = "0px 0px 15px rgba(0, 0, 0, 0.2)";

        let texttag = document.createElement("p");
        texttag.innerHTML = widget.widgetName;
        texttag.style.fontFamily    = "Raleway";
        texttag.style.fontWeight    = "600";
        texttag.style.color         = "#fafafa";
        texttag.style.letterSpacing = "0.05em";
        texttag.style.textAlign     = "center";
        texttag.style.position      = "absolute";
        texttag.style.top           = "50%";
        texttag.style.left          = "0";
        texttag.style.right         = "0";
        texttag.style.transform     = "translate(0, -50%)";

        domelement.appendChild(texttag);
        domelement.onmousedown = function(event: any) {
            if (event.which == 2) {
                // Mark the widget as deleted
                event.preventDefault();
                widget.deleted = true;
                let widgetDomElement: HTMLElement = document.getElementById(widget.domid)!;
                widgetDomElement.remove();
            }
        };
        divMainArea.appendChild(domelement);
        makeinteractive("#" + widget.domid);
    });
}

function makeinteractive(id: string) {
    // Lets the user resize and move the widget...
    let resizablesnaptarget;
    let draggablesnaptarget;
    let edges;
    let restrictsize;
    if (!isvertical) {
        resizablesnaptarget = interact.createSnapGrid({ x: Math.round(document.documentElement.clientWidth / 100), y: null });
        draggablesnaptarget = interact.createSnapGrid({ x: Math.round(document.documentElement.clientWidth / 100), y: null });
        restrictsize = {
            min: { width: 20, height: document.documentElement.clientHeight },
            max: { height: document.documentElement.clientHeight }
        };
        edges = { left: true, right: true, bottom: false, top: false };
    } else {
        resizablesnaptarget = interact.createSnapGrid({ y: Math.round(document.documentElement.clientWidth / 100), x: null });
        draggablesnaptarget = interact.createSnapGrid({ y: Math.round(document.documentElement.clientWidth / 100), x: null });
        restrictsize = {
            min: { height: 20, width: document.documentElement.clientWidth },
            max: { width: document.documentElement.clientWidth }
        };
        edges = { left: false, right: false, bottom: true, top: true };
    }

    interact(id)
        .draggable({
            snap: {
                targets: [
                    draggablesnaptarget
                ],
                range: Infinity,
                relativePoints: [{ x: 0, y: 0 }]
            },
            onmove: (window as any).dragMoveListener,
            restrict: {
                restriction: 'parent',
                elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
            },
        })
        .resizable({
            snap: {
                targets: [
                    resizablesnaptarget
                ],
                range: Infinity,
                relativePoints: [{ x: 0, y: 0 }]
            },
            // resize from all edges and corners
            edges: edges,

            // keep the edges inside the parent
            restrictEdges: {
                outer: 'parent',
                endOnly: true,
            },

            // minimum size
            restrictSize: restrictsize,

            inertia: true,
        })
        .on('resizemove', function (event: any) {
            var target = event.target,
                x = (parseFloat(target.getAttribute('data-x')) || 0),
                y = (parseFloat(target.getAttribute('data-y')) || 0);

            // update the element's style
            target.style.width = event.rect.width + 'px';
            target.style.height = event.rect.height + 'px';

            // translate when resizing from top or left edges
            x += event.deltaRect.left;
            y += event.deltaRect.top;

            target.style.webkitTransform = target.style.transform =
                'translate(' + x + 'px,' + y + 'px)';

            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
        });
}

function dragMoveListener(event: any) {
    if (!isvertical) {
        var target = event.target,
            // keep the dragged position in the data-x/data-y attributes
            x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;

        // translate the element
        target.style.webkitTransform =
            target.style.transform =
            'translate(' + x + 'px, 0px)';

        // update the posiion attributes
        target.setAttribute('data-x', x);
    } else {
        var target = event.target,
            // keep the dragged position in the data-x/data-y attributes
            y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

        // translate the element
        target.style.webkitTransform =
            target.style.transform =
            'translate(0px, ' + y + 'px)';

        // update the posiion attributes
        target.setAttribute('data-y', y);
    }
}

(window as any).dragMoveListener = dragMoveListener;

function applychanges() {
    // Save the changes to the db.
    db.prepare("DROP TABLE IF EXISTS `panel" + panelid + "`").run();
    db.prepare("CREATE TABLE panel" + panelid + " (widget, position, width)").run();

    widgetssettings.forEach(widget => {
        if (!widget.deleted) {
            let widgetDomElement: HTMLElement = document.getElementById(widget.domid)!;

            let newPosition;
            let newSize;
            if (!isvertical) {
                let xOffset = Math.round((parseFloat(widgetDomElement.getAttribute('data-x')!) || 0) / document.documentElement.clientWidth * 100);
                let newX: number = widget.widgetPosition + xOffset;
                let newWidth = Math.round(widgetDomElement.clientWidth / document.documentElement.clientWidth * 100);
                newPosition = newX;
                newSize = newWidth;
            } else {
                let yOffset = Math.round((parseFloat(widgetDomElement.getAttribute('data-y')!) || 0) / document.documentElement.clientHeight * 100);
                let newY: number = widget.widgetPosition + yOffset;
                let newHeight = Math.round(widgetDomElement.clientHeight / document.documentElement.clientHeight * 100);
                newPosition = newY;
                newSize = newHeight;
            }

            db.prepare("INSERT INTO `panel" + panelid + "` VALUES('" + widget.widgetName + "', '" + newPosition + "', '" + newSize + "')").run();

            interact('#' + widget.domid).unset();
        }
    });
}