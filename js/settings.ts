import bettersqlite3 from 'better-sqlite3';
import * as path from 'path';
import { remote, ipcRenderer } from 'electron';
const app = remote.app;
const displayutils = require('../js/displayutils');


let panels: Array<PanelConfig> = [];
let db: bettersqlite3;
let currentcount: number;

const positionradioid = ["positionradio1", "positionradio2", "positionradio3", "positionradio4"]

interface PanelConfig {
    table: number;
    position: number;
    display: number;
    height: number;
    background: string;
    listElement: HTMLElement;
}
interface DisplayInfo {
    name: string;
    x: number;
    y: number;
    width: number;
    height: number;
}

let currentlyactivepanel: PanelConfig;

function loadDisplays() {
    let idcounter = 1;
    // Get the displays...
    displayutils.getDisplays(function (res: DisplayInfo) {
        // Create list entries.
        let listEntry = document.createElement("li");

        let radioInput = document.createElement("input");
        radioInput.onchange = savePanel;
        radioInput.id = "displayradio" + idcounter;
        radioInput.name = "displayradio";
        radioInput.type = "radio";
        if (idcounter == 1) { radioInput.checked = true; }
        listEntry.appendChild(radioInput);

        let radioLabel = document.createElement("label");
        radioLabel.setAttribute("for", "displayradio" + idcounter);
        radioLabel.innerHTML = res.name;
        listEntry.appendChild(radioLabel);

        document.getElementById("displayselector")!.appendChild(listEntry);

        idcounter++;
    });
}

function loadPanels() {
    // Open the sqlite-db
    var configpath = path.join(app.getPath("appData"), "instantpanel.db");

    db = new bettersqlite3(configpath);
    
    let result = db.prepare("SELECT * FROM panels").all();

    result.forEach(row => {
        // Create a li-element in the panel selector
        let listEntry: HTMLElement = document.createElement("li");
        listEntry.innerHTML = "Panel " + row.tbl;
        document.getElementsByClassName("panelselector")[0].insertBefore(listEntry, document.getElementsByClassName("addpanel")[0]);
        // Add the panel to the array
        let panelObject: PanelConfig = {
            table: parseInt(row.tbl),
            position: parseInt(row.position),
            display: parseInt(row.display),
            height: parseInt(row.height),
            background: row.background,
            listElement: listEntry
        };
        panels.push(panelObject);
        listEntry.onclick = function() {
            openPanel(panelObject)
        };
        listEntry.onmousedown = function(e) {
            if (e.which == 2) {
                e.preventDefault();
                deletePanel(panelObject);
            }
        };
    });
    if (panels.length != 0) {
        currentcount = panels[panels.length - 1].table;
    } else {
        currentcount = 0;
    }
}

function deactivateSettings() {
    document.getElementById("mainsettings")!.style.display = "none";
}

function createPanel() {
    // New highest id
    currentcount++;
    // Create an entry in "panels" and create a table for that panel.
    db.prepare("INSERT INTO `panels` VALUES('" + currentcount +  "', '0', '0', '35', 'rgba(0, 0, 0, 0.5)')").run();
    db.prepare("CREATE TABLE panel" + currentcount + " (widget, position, width)").run();
    // Create a li-element in the panel selector.
    let listEntry: HTMLElement = document.createElement("li");
    listEntry.innerHTML = "Panel " + currentcount;
    document.getElementsByClassName("panelselector")[0].insertBefore(listEntry, document.getElementsByClassName("addpanel")[0]);
    // Add the panel to the array.
    let panelObject: PanelConfig = {
        table: currentcount,
        position: 0,
        display: 0,
        height: 35,
        background: "rgba(0, 0, 0, 0.5)",
        listElement: listEntry
    };
    panels.push(panelObject);
    listEntry.onclick = function() {
        openPanel(panelObject)
    };
    listEntry.onmousedown = function(e) {
        if (e.which == 2) {
            e.preventDefault();
            deletePanel(panelObject);
        }
    };
    // Open the new panel in the settings.
    updatePanels();
    openPanel(panels[panels.length - 1]);
}

function activatePanel(panel: PanelConfig) {
    let panelselectors = document.getElementsByClassName("panelselector")[0].children;
    // Only one panel can be active at the same time...
    for (var i = 0; i < (panelselectors.length - 1); i++) {
        panelselectors[i].className = "";
    }
    
    panel.listElement.className = "active";
}

function openPanel(panel: PanelConfig) {
    document.getElementById("mainsettings")!.style.display = "block";
    currentlyactivepanel = panel;
    activatePanel(panel);

    // Check the selected panel position.
    let positionradiobtn = document.getElementById(positionradioid[panel.position]);
    (positionradiobtn as HTMLInputElement).checked = true;
    // Check the selected display.
    let displayradiobtn = document.getElementById("displayradio" + (panel.display + 1));
    (displayradiobtn as HTMLInputElement).checked = true;
    // Check the selected height.
    let heightradiobtn: HTMLElement;
    (document.getElementById("customheightinput") as HTMLInputElement).value = "";
    switch(panel.height) {
        case 35:
            heightradiobtn = document.getElementById("heightradio1")!;
            break;
        case 50:
            heightradiobtn = document.getElementById("heightradio2")!;
            break;
        case 75:
            heightradiobtn = document.getElementById("heightradio3")!;
            break;
        default:
            (document.getElementById("customheightinput") as HTMLInputElement).value = panel.height.toString();
            heightradiobtn = document.getElementById("heightradio4")!;
    }
    (heightradiobtn as HTMLInputElement).checked = true;
    // Display the selected color.
    (document.getElementsByClassName("colorbutton")[0] as HTMLElement).style.backgroundColor = panel.background;
    // Activate edit mode.
    (document.getElementById("editmodecheck") as HTMLInputElement).checked = true;
    ipcRenderer.send('activateedit', panel.table);
}

function updatePanels() {
    ipcRenderer.send('update', 1);
}

function deletePanel(panel: PanelConfig) {
    // Remove the panel from the panel selector.
    document.getElementsByClassName("panelselector")[0].removeChild(panel.listElement);
    // Remove the panel from the array.
    panels.splice(panels.indexOf(panel), 1);
    if (panels.length != 0) {
        // Set current count.
        currentcount = panels[panels.length - 1].table;
        // If the active panel was deleted, make the first panel active.
        if (currentlyactivepanel != undefined && currentlyactivepanel == panel) {
            openPanel(panels[0]);
        }
    } else {
        currentcount = 0;
        deactivateSettings();
    }
    // Actually delete the panel from the db.
    db.prepare("DELETE FROM `panels` WHERE tbl='" + panel.table + "'").run();
    db.prepare("DROP TABLE panel" + panel.table).run();
    updatePanels();
}

function savePanel() {
    // Get properties.
    let checkedpositionradio = document.querySelector('input[name = "positionradio"]:checked');
    let newpositionvalue = positionradioid.indexOf((checkedpositionradio as HTMLInputElement).id);
    console.log("New position: " + newpositionvalue);

    let checkeddisplayradio = document.querySelector('input[name = "displayradio"]:checked');
    let newdisplayvalue = parseInt((checkeddisplayradio as HTMLInputElement).id.substr(12)) - 1;
    console.log("New display: " + newdisplayvalue);

    let checkedheightradio = document.querySelector('input[name = "heightradio"]:checked');
    let newheightvalue = 10;
    switch((checkedheightradio as HTMLInputElement).id) {
        case "heightradio1":
            newheightvalue = 35;
            break;
        case "heightradio2":
            newheightvalue = 50;
            break;
        case "heightradio3":
            newheightvalue = 75;
            break;
        case "heightradio4":
            if ((document.getElementById("customheightinput") as HTMLInputElement).value != "") {
                newheightvalue = parseInt((document.getElementById("customheightinput") as HTMLInputElement).value);
            } else {
                newheightvalue = currentlyactivepanel.height;
            }
            break;
    }
    console.log("New height: " + newheightvalue);

    let backgroundbutton = (document.getElementsByClassName("colorbutton")[0] as HTMLElement);
    let newbackgroundvalue = backgroundbutton.style.backgroundColor!;
    console.log("New background: " + newbackgroundvalue);

    // Update the array.
    let arrayindex = panels.indexOf(currentlyactivepanel);
    currentlyactivepanel.position = newpositionvalue;
    currentlyactivepanel.display = newdisplayvalue;
    currentlyactivepanel.height = newheightvalue;
    currentlyactivepanel.background = newbackgroundvalue;
    panels[arrayindex] = currentlyactivepanel;

    // Update the database.
    db.prepare("UPDATE `panels` SET position='" + newpositionvalue + "', " +
                "display='" + newdisplayvalue + "', " + 
                "height='" + newheightvalue + "', " +
                "background='" + newbackgroundvalue + "' WHERE tbl='" + currentlyactivepanel.table + "'").run();

    // Notify the main process that the configuration has changed.
    updatePanels();
}

function toggleeditmode() {
    if ((document.getElementById("editmodecheck") as HTMLInputElement).checked) {
        // Activate edit mode.
        ipcRenderer.send('activateedit', currentlyactivepanel.table);
    } else {
        // Deactivate edit mode.
        ipcRenderer.send('deactivateedit', currentlyactivepanel.table);
    }
}