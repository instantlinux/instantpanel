import * as path from 'path';
import * as url from 'url';
import * as fs from 'fs';
import {app, BrowserWindow, protocol, ipcMain, IpcMessageEvent} from 'electron';
import bettersqlite3 from 'better-sqlite3';
import * as gala_blur from 'gala-blur-implementation';
const displayutils = require('./js/displayutils');



interface DisplayInfo {
    name: string;
    x: number;
    y: number;
    width: number;
    height: number;
}

let wins: Electron.BrowserWindow[] = [];
let panels: Array<number> = [ -1 ];
let previouslyactivepanel: number = -1;

function createWindow() {
    protocol.unregisterProtocol('', () => { // Needed for creating a transparent window (https://github.com/electron/electron/issues/2170)
        var configpath = path.join(app.getPath("appData"), "instantpanel.db");

        let db: bettersqlite3;

        if (!fs.existsSync(configpath)) {
            if (fs.existsSync("/usr/share/instantpanel/sample.db")) {
                fs.copyFileSync("/usr/share/instantpanel/sample.db", configpath);
                db = new bettersqlite3(configpath);
            } else {
                db = new bettersqlite3(configpath);
                db.prepare("CREATE TABLE IF NOT EXISTS panels (tbl, position, display, height, background)").run();
            }
        } else {
            db = new bettersqlite3(configpath);
        }
        let result = db.prepare("SELECT * FROM panels").all();

        if (result.length > 0) {
            let displays: Array<DisplayInfo> = [];
            
            if (process.argv[2] == "--settings" || process.argv[2] == "-s") {
                openSettings(0);    
            }
            // Get the displays...
            displayutils.getDisplays(function (res: DisplayInfo) {
                displays.push(res);
                result.forEach(row => {
                    if (parseInt(row.display) == displays.length - 1) {
                        let index = wins.length;

                        let params: Array<number> = [];

                        panels[index] = parseInt(row.tbl);
                        switch(parseInt(row.position)) {
                            case 0:
                                wins[index] = new BrowserWindow({
                                    width: res.width,
                                    height: parseInt(row.height),
                                    x: res.x,
                                    y: res.y,
                                    transparent: true,
                                    frame: false,
                                    movable: false,
                                    resizable: false,
                                    type: 'dock',
                                    title: "instantpanel" + row.tbl
                                });
                                params = [res.width, 0, parseInt(row.height), 0, 
                                    res.y, res.y + parseInt(row.height), 0, 0, 
                                    res.x, res.x + res.width, 0, 0];
                                break;
                            case 1:
                                wins[index] = new BrowserWindow({
                                    width: res.width,
                                    height: parseInt(row.height),
                                    x: res.x,
                                    y: res.y + res.height - parseInt(row.height),
                                    transparent: true,
                                    frame: false,
                                    movable: false,
                                    resizable: false,
                                    type: 'dock',
                                    title: "instantpanel" + row.tbl
                                });
                                params = [res.width, 0, 0, parseInt(row.height), 
                                    res.y + res.height - parseInt(row.height), res.y + res.height, 0, 0, 
                                    0, 0, res.x, res.x + res.width];
                                break;
                            case 2:
                                wins[index] = new BrowserWindow({
                                    width: parseInt(row.height),
                                    height: res.height,
                                    x: res.x,
                                    y: res.y,
                                    transparent: true,
                                    frame: false,
                                    movable: false,
                                    resizable: false,
                                    type: 'dock',
                                    title: "instantpanel" + row.tbl
                                });
                                params = [parseInt(row.height), 0, res.height, 0, 
                                    res.y, res.y + res.height, 0, 0, 
                                    res.x, res.x + parseInt(row.height), 0, 0];
                                break;
                            default:
                                wins[index] = new BrowserWindow({
                                    width: parseInt(row.height),
                                    height: 1080,
                                    x: res.x + res.width - parseInt(row.height),
                                    y: res.y,
                                    transparent: true,
                                    frame: false,
                                    movable: false,
                                    resizable: false,
                                    type: 'dock',
                                    title: "instantpanel" + row.tbl
                                });
                                params = [0, parseInt(row.height), res.height, 0, 
                                    0, 0, res.y, res.y + res.height, 
                                    res.x + res.width - parseInt(row.height), res.x + res.width, 0, 0];
                        }


                        // wins[index].webContents.openDevTools();
                        wins[index].setSkipTaskbar(true);
                        wins[index].setAlwaysOnTop(true);

                        displayutils.getWindowID("instantpanel" + row.tbl).then(function (wid: number) {
                            displayutils.setStrutValues(wid, 
                                params[0], params[1], params[2], params[3], 
                                params[4], params[5], params[6], params[7], 
                                params[8], params[9], params[10], params[11], function() {
                                
                                gala_blur.BlurWindow(wid.toString());
                                
                                wins[index].loadURL(url.format({
                                    pathname: path.join(__dirname, "html/panel.html"),
                                    protocol: 'file:',
                                    slashes: true
                                }));
        
                                wins[index].once('page-title-updated', () => {
                                    wins[index].webContents.send('init', parseInt(row.tbl));
                                });
        
                                wins[index].on('closed', () => {
                                    wins[index] = null!;
                                });

                            });
                        });
                    }
                });
            });
        } else {
            console.log("No panels found! Opening settings...");
            openSettings(0);
        }
    });
}

function openSettings(index: number) {
    wins[index] = new BrowserWindow({
        width: 600,
        height: 500,
        transparent: true
    });

    wins[index].loadURL(url.format({
        pathname: path.join(__dirname, "html/settings.html"),
        protocol: 'file:',
        slashes: true
    }));

    wins[index].setMenu(null);

    wins[index].on('closed', () => {
        if (previouslyactivepanel != -1) {
            wins[previouslyactivepanel].webContents.send('editMode', false);
        }
        wins[index] = null!;
    });
} 

app.disableHardwareAcceleration();
app.commandLine.appendSwitch('enable-transparent-visuals');
app.on('ready', createWindow);

ipcMain.on('update', function(event: IpcMessageEvent, arg: any) {
    console.log("Updating is not implemented yet.");
});
ipcMain.on('activateedit', function(event: IpcMessageEvent, arg: any) {
    if (previouslyactivepanel != -1) {
        wins[previouslyactivepanel].webContents.send('editMode', false);
    }
    wins[panels.indexOf(arg)].webContents.send('editMode', true);
    previouslyactivepanel = panels.indexOf(arg);
    console.log("Activating edit mode for " + arg);
});
ipcMain.on('deactivateedit', function(event: IpcMessageEvent, arg: any) {
    wins[panels.indexOf(arg)].webContents.send('editMode', false);
    previouslyactivepanel = -1;
    console.log("Deactivating edit mode: " + arg);
});

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    app.quit();
});

app.on('activate', () => {
    wins.forEach(win => {
        if (win === null) {
            createWindow();
        }
    });
});